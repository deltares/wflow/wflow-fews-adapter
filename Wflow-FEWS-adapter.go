// Reads the start and end date from a Delft-FEWS runfile XML.
// These dates are then placed in a Wflow TOML config file.
// Martijn Visser, Deltares, 2020-10

package main

import (
	"encoding/xml"
	"flag"
	"fmt"
	"io/ioutil"
	"log"
	"os"

	"text/template"

	toml "github.com/pelletier/go-toml"
)

const diagHeader = `<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<Diag xmlns="http://www.wldelft.nl/fews/PI" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.wldelft.nl/fews/PI HTTP://fews.wldelft.nl/schemas/version1.0/pi-schemas/pi_diag.xsd" version="1.2">
`
const diagFooter = "</Diag>\n"

// create diagnostics file and write header
func setupDiagnostics(pathDiag string) *os.File {
	diag, err := os.Create(pathDiag)
	if err != nil {
		log.Panic(err) // cannot log to diagnostics since cannot create diagnostics
	}
	diag.Write([]byte(diagHeader))
	return diag
}

// check error and log to diagnostic then panic
func check(e error, diag *os.File) {
	if e != nil {
		msg := fmt.Sprint(e)
		logDiagnostic(msg, 1, diag) // level: error
		log.Panic(msg)
	}
}

// write a diagnostic message to the XML file with specified error level, 1: error, 2: warn
// see https://publicwiki.deltares.nl/display/FEWSDOC/Pi+Diagnostics
func logDiagnostic(msg string, level int, diag *os.File) {
	line := Diagnostic{level, msg}
	tmpl, err := template.New("test").Parse("	<line level=\"{{.Level}}\" description=\"{{.Description}}\"/>\n")
	if err != nil {
		log.Panic(err)
	}
	err = tmpl.Execute(diag, line)
	if err != nil {
		log.Panic(err)
	}
}

// write XML footer, run deferred to ensure XML is valid
func writeXMLFooter(diag *os.File) {
	diag.Write([]byte(diagFooter))
	diag.Close()
}

// Diagnostic used to fill template for XML
type Diagnostic struct {
	Level       int
	Description string
}

func main() {

	pathRunInfo := flag.String("run", "run_info.xml",
		"Delft-FEWS runfile XML, which provides the start and end date")

	pathTomlTemplate := flag.String("template", "wflow_config_template.toml",
		"Wflow TOML config file, on which the output TOML is based")

	pathTomlOutput := flag.String("output", "wflow_config.toml",
		"path to output Wflow TOML config file, with updated start and end times")

	flag.Parse()

	// Set up Delft-FEWS PI diagnostics XML file
	diag := setupDiagnostics("diagnostics.xml")
	defer writeXMLFooter(diag)

	// date attribute
	type DateTime struct {
		Date string `xml:"date,attr"`
		Time string `xml:"time,attr"`
	}

	// startDateTime and endDateTime elements
	type Run struct {
		StartDateTime DateTime `xml:"startDateTime"`
		EndDateTime   DateTime `xml:"endDateTime"`
	}

	// initialize empty struct
	run := new(Run)

	// read run_info.xml to []byte
	runInfo, err := ioutil.ReadFile(*pathRunInfo)
	check(err, diag)

	// parse XML file to the run struct
	err = xml.Unmarshal(runInfo, &run)
	check(err, diag)

	// read TOML config template
	config, err := toml.LoadFile(*pathTomlTemplate)
	check(err, diag)

	config.Set("starttime", run.StartDateTime.Date+" "+run.StartDateTime.Time)
	config.Set("endtime", run.EndDateTime.Date+" "+run.EndDateTime.Time)

	b, err := toml.Marshal(config)
	check(err, diag)
	err = ioutil.WriteFile(*pathTomlOutput, b, 0644)
	check(err, diag)

}
