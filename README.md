# Wflow adapter for Delft-FEWS

**This repository is archived. Wflow can be used without an adapter, see the [Wflow docs](https://deltares.github.io/Wflow.jl/stable/user_guide/additional_options/#run_fews).**

This is an adapter for the hydrological model [Wflow](https://github.com/Deltares/Wflow.jl),
to allow it to be easily run from [Delft-FEWS](https://oss.deltares.nl/web/delft-fews) in an
operational forecasting system.

Other model adapters for Delft-FEWS can be found in the [Delft-FEWS
documentation](https://publicwiki.deltares.nl/display/FEWSDOC/Models+linked+to+Delft-Fews).

This adapter is written in [Go](https://golang.org/), because it is easy to create
standalone executables from it for any platform. Just install Go and run `go build` to do
so.

## Usage

Wflow works with a [TOML](https://toml.io/) configuration file. This adapter works by taking
a TOML file for an existing model as a template. Delft-FEWS provides the requested start and
end dates in an [XML run
file](https://publicwiki.deltares.nl/display/FEWSDOC/05+General+Adapter+Module#id-05GeneralAdapterModule-exportRunFileActivity).
The adapter reads this information from the XML file, and writes a new TOML file that
combines the template TOML with the information from the XML file. It supports command line
options to point to the needed files. These can be seen by running `Wflow-FEWS-adapter
--help`, and are also pasted below:

```
Usage of Wflow-FEWS-adapter
  -output string
        path to output Wflow TOML config file, with updated start and end times (default "wflow_config.toml")
  -run string
        Delft-FEWS runfile XML, which provides the start and end date (default "run_info.xml")
  -template string
        Wflow TOML config file, on which the output TOML is based (default "wflow_config_template.toml")
```
